#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_a55_a55.mk

COMMON_LUNCH_CHOICES := \
    lineage_a55_a55-user \
    lineage_a55_a55-userdebug \
    lineage_a55_a55-eng
